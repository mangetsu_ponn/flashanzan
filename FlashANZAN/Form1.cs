﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FlashANZAN
{
    public partial class Form1 : Form
    {
        int count = 0;
        int difficulty = 100;
        int sum = 0;
        int answer;
        bool conversion;
        int num;

        public Form1()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            int speed;
 
            if (rb_superHighSpeed.Checked)
            {
                speed = 300;
            }
            else if (rb_highSpeed.Checked)
            {
                speed = 500;
            }
            else if (rb_lowSpeed.Checked)
            {
                speed = 2000;
            }
            else if (rb_superLowSpeed.Checked)
            {
                speed = 3000;
            }
            else
            {
                speed = 1000;
            }

            if (rb_toThreeDigits.Checked)
            {
                difficulty = 1000;
            }
            else if (rb_oneDigit.Checked)
            {
                difficulty = 10;
            }
            else
            {
                difficulty = 100;
            }

            timer1.Interval = speed;
            timer1.Elapsed += Timer1_Elapsed;
            timer1.Enabled = true;
        }

        private void Timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            num = new Random().Next(1, difficulty);
            this.lbl_numDisp.Text = num.ToString();
            sum += num;

            count++;

            if(count == (cb_calcTimes.SelectedIndex + 3))
            {
                timer1.Enabled = false;
                timer1.Dispose();
            }
        }

        private void AnswerCheckButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tb_answer.Text))
            {
                MessageBox.Show("解答を整数で入力して下さい", "確認");
            }
            else
            {
                conversion = int.TryParse(tb_answer.Text, out answer);
            
                if (!conversion)
                {
                    MessageBox.Show("入力が不正です\n整数を入力して下さい", "注意");
                }
                else
                {
                    if (sum == answer)
                    {
                        MessageBox.Show("おめでとう！\n正解です", "正解");
                    }
                    else
                    {
                        MessageBox.Show("残念！\n不正解です\n正解は"+ sum +"です", "不正解");
                    }
                }
            }
        }
    }
}
